﻿#ifndef LIB_H
#define LIB_H
#include "general.h"
class Lib
{
    QList<Book> bookList;
    QString libName;
public:
    Lib();
    QList<Book> getBookList() const;
    Book operator[](int i)const;
    Lib operator [](QString s)const;//find books by Author Name or Publisher
    int findBook(const Book &b)const;
    Lib& operator+=(const Book &b);
    Lib &operator+=(const Lib &lib);
    Lib &operator -=(const Book &b);
    Lib &operator -=(const Lib &lib);
    int count()const;

};

#endif // LIB_H
