#ifndef BOOK_H
#define BOOK_H
#include "general.h"

class Book
{
QString Author,Title,Publisher;
int pageCount;
public:
    Book();
    Book(QString Author, QString Title, QString Publisher, int pageCount);

    bool operator ==(const Book& s) const;

    QString getAuthor() const;
    void setAuthor(const QString &value);
    QString getTitle() const;
    void setTitle(const QString &value);
    QString getPublisher() const;
    void setPublisher(const QString &value);
    int getPageCount() const;
    void setPageCount(int value);
};

#endif // BOOK_H
