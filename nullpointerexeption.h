#ifndef NULLPOINTEREXEPTION_H
#define NULLPOINTEREXEPTION_H
#include "general.h"
class NullPointerExeption : public QException
{
public:
    NullPointerExeption();
};

#endif // NULLPOINTEREXEPTION_H
