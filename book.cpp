#include "general.h"

QString Book::getAuthor() const
{
    return Author;
}

void Book::setAuthor(const QString &value)
{
    Author = value;
}

QString Book::getTitle() const
{
    return Title;
}

void Book::setTitle(const QString &value)
{
    Title = value;
}

QString Book::getPublisher() const
{
    return Publisher;
}

void Book::setPublisher(const QString &value)
{
    Publisher = value;
}

int Book::getPageCount() const
{
    return pageCount;
}

void Book::setPageCount(int value)
{
    pageCount = value;
}
Book::Book()
{
}

Book::Book(QString Author, QString Title, QString Publisher,int pageCount):Author(Author),Title(Title),Publisher(Publisher),pageCount(pageCount)
{

}

bool Book::operator ==(const Book &s) const
{
    return(Author==s.Author&&Title==s.Title&&Publisher==s.Publisher&&pageCount==s.pageCount);
}
