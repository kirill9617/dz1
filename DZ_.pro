QT       += core
QT       -= gui

TARGET = DZ_
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app
SOURCES += main.cpp \
    book.cpp \
    lib.cpp \
    nullpointerexeption.cpp

HEADERS += \
    book.h \
    general.h \
    lib.h \
    nullpointerexeption.h

