﻿#include "general.h"

QList<Book> Lib::getBookList() const
{
    return bookList;
}

Book Lib::operator[](int i)const
{
    return bookList[i];
}

Lib Lib::operator [](QString s) const
{
    Lib l;
    foreach (Book b, bookList) {
        if(b.getAuthor()==s||b.getTitle()==s||b.getPublisher()==s)
            l+=b;
    }
    return l;
}

int Lib::findBook(const Book &b) const
{
    return bookList.lastIndexOf(b);
}

Lib &Lib::operator+=(const Book &b)
{
    bookList.append(b);
    return *this;
}
Lib &Lib::operator +=(const Lib& lib){
    bookList.append(lib.getBookList());
    return *this;
}

Lib &Lib::operator -=(const Book &b)
{
    int i=bookList.lastIndexOf(b);
    if(!~i)
        bookList.erase(bookList.begin()+ i);
    return *this;
}

Lib &Lib::operator -=(const Lib &lib)
{
    foreach (Book b, lib.bookList) {
        operator -=(b);
    }
    return *this;
}

int Lib::count() const
{
    return bookList.count();
}
Lib::Lib()
{
}
